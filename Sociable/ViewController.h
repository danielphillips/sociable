//
//  ViewController.h
//  Sociable
//
//  Created by Daniel Phillips on 18/10/2012.
//  Copyright (c) 2012 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>
#import "Facebook.h"


@interface ViewController : UIViewController<UIActionSheetDelegate, FBDialogDelegate>
- (IBAction)shareAction:(id)sender;

@end
