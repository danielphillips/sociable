//
//  AppDelegate.h
//  Sociable
//
//  Created by Daniel Phillips on 18/10/2012.
//  Copyright (c) 2012 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@property (strong, nonatomic) Facebook *facebook;

- (void)openSession;

@end
