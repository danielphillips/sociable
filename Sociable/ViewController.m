//
//  ViewController.m
//  Sociable
//
//  Created by Daniel Phillips on 18/10/2012.
//  Copyright (c) 2012 Daniel Phillips. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareAction:(id)sender {
    if(NSClassFromString(@"UIActivityViewController")){
        
        NSString *textToShare = @"Hello World!";
        UIImage *imageToShare = [UIImage imageNamed:@"IMG_5423.jpg"];
        NSArray *activityItems = @[textToShare, imageToShare];
        
        UIActivityViewController *activityVC =
        [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                          applicationActivities:nil];
        [self presentViewController:activityVC animated:YES completion:nil];
    }else{
        UIActionSheet *shareSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                delegate:self
                                                       cancelButtonTitle:@"Cancel"
                                                  destructiveButtonTitle:nil
                                                       otherButtonTitles:@"Facebook", @"Twitter", nil];
        
        [shareSheet showInView:self.view];
        [shareSheet release];
    }
    
}

- (void)shareLegacyTwitter{
    if( NSClassFromString(@"TWTweetComposeViewController") ){
        // We don't have the Social framework, work with the Twitter framework
        // iOS 5 only will use this
        
        if( [TWTweetComposeViewController canSendTweet] ){
            TWTweetComposeViewController *twitterCompose = [[TWTweetComposeViewController alloc] init];
            
            NSString *textToShare = @"Hello World!";
            UIImage *imageToShare = [UIImage imageNamed:@"IMG_5423.jpg"];
            
            
            [twitterCompose setInitialText:textToShare];
            [twitterCompose addImage:imageToShare];
            
            twitterCompose.completionHandler = ^(TWTweetComposeViewControllerResult result){
                // Handle result, dismiss view controller
                [self dismissViewControllerAnimated:YES
                                         completion:nil];
            };
            [self presentViewController:twitterCompose
                               animated:YES
                             completion:nil];
        }else{
            // the user hasn't go Twitter set up on their device.
            
            UIAlertView *alertNoAccount = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                     message:@"You don't have Twitter set up on your device"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
            [alertNoAccount show];
            [alertNoAccount release];
        }
    }
}

- (void)shareLegacyFacebook{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    
    if(!FBSession.activeSession.isOpen){
        [delegate openSession];
    } else {
        NSLog(@"dialog");
        
        UIImage *img = [UIImage imageNamed:@"IMG_5423.jpg"];
        
        // if it is available to us, we will post using the native dialog
        [self performPublishAction:^{
            
            [FBRequestConnection startForUploadPhoto:img
                                   completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                       
                                   }];
            
        }];
        
        return;
    }
}

- (void) performPublishAction:(void (^)(void)) action {
    // we defer request for permission to post to the moment of post, then we check for the permission
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // if we don't already have the permission, then we request it now
        [FBSession.activeSession reauthorizeWithPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                                   defaultAudience:FBSessionDefaultAudienceFriends
                                                 completionHandler:^(FBSession *session, NSError *error) {
                                                     if (!error) {
                                                         action();
                                                     }
                                                     //For this example, ignore errors (such as if user cancels).
                                                 }];
    } else {
        action();
    }
    
}


#pragma mark - UIActionSheetDelegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            // Facebook
            [self shareLegacyFacebook];
            break;
        case 1:
            // Twitter
            [self shareLegacyTwitter];
            break;
            
        default:
            break;
    }
}


@end
